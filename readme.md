# ScannerSpringBoot Project

## Overview

ScannerSpringBoot is a comprehensive Spring Boot application designed for scanning directories and files. It supports local and AWS S3 bucket scanning, providing detailed insights into file structures, including statistics on scan durations and file types. This project includes Dockerization for easy deployment and integration with PostgreSQL for robust data management.

## Features

- **Local and S3 Scanning**: Flexible scanning capabilities to work with local file systems or AWS S3 buckets.
- **Customizable Scanning Criteria**: Users can specify maximum depth, file counts, name filters, and type filters to tailor the scanning process.
- **Statistical Insights**: Generates statistics on the number of files scanned, average scan durations, and more.
- **Docker Support**: Dockerization of both the application and PostgreSQL database for streamlined deployment.
- **Comprehensive API**: A well-defined RESTful API supports creating, duplicating, replaying, and comparing scans, alongside fetching scan statistics and file details.
- **Swagger Documentation**: Integrated Swagger UI for easy exploration and testing of the API endpoints.

## Getting Started

To run the project, Docker and Docker Compose are required. The application can be launched using Docker Compose, which sets up the application and the PostgreSQL database. Specific instructions on building and running the containers are provided in the Docker documentation within the project.

### Prerequisites

- java 17
- maven 8
- docker

### Installation

1. Clone the repository
   ```sh
   git clone
   ```
2. Build the application
   ```sh
   ./mvnw package
   ```
3. Build the Docker image
   ```sh
   docker-compose up --build
   ```

## Usage

The application offers a Swagger UI for interacting with the API. Examples include creating scans, duplicating scans with modified criteria, and fetching scan results or statistics. Additionally, it's capable of scanning AWS S3 buckets by configuring the necessary AWS credentials.

The coverage of tests can be found in ./target/site/jacoco/index.html

## Authors

- **Riyad Chamekh**
- **Jules Dumouchel**