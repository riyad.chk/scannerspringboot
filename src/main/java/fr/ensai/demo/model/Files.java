package fr.ensai.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.time.LocalDateTime;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // Ignore certaines propriétés non nécessaires dans le JSON
public class Files {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long filesid;

    @JsonProperty("fileName") 
    private String namefile;

    @JsonProperty("fileSize")
    private int sizefile;

    @JsonProperty("fileType")
    private String typefile;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss") // Formate la date dans le JSON
    private LocalDateTime lastmodified;

    @JsonProperty("parentFolder")
    private String parentfolder;


    public Files() {
    }

    public Files(String namefile, int sizefile, String typefile, LocalDateTime lastmodified, String parentfolder) {
        this.namefile = namefile;
        this.sizefile = sizefile;
        this.typefile = typefile;
        this.lastmodified = lastmodified;
        this.parentfolder = parentfolder;
    }

    public Long getId() {
        return filesid;
    }

    public void setId(Long id) {
        this.filesid = id;
    }

    public String getNamefile() {
        return namefile;
    }

    public void setNamefile(String namefile) {
        this.namefile = namefile;
    }

    public int getSizefile() {
        return sizefile;
    }

    public void setSizefile(int sizefile) {
        this.sizefile = sizefile;
    }

    public String getTypefile() {
        return typefile;
    }

    public void setTypefile(String typefile) {
        this.typefile = typefile;
    }

    public LocalDateTime getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(LocalDateTime lastmodified) {
        this.lastmodified = lastmodified;
    }

    public String getParentfolder() {
        return parentfolder;
    }

    public void setParentfolder(String parentfolder) {
        this.parentfolder = parentfolder;
    }
}
