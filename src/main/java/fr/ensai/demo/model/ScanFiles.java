package fr.ensai.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "scan_files")
public class ScanFiles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer scanId; // Java convention: variable names should start with a lowercase letter

    private Integer filesScan; // Assuming this is the correct name

    public ScanFiles() {
    }

    // Corrected constructor, getters and setters according to Java naming conventions
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScanId() {
        return scanId;
    }

    public void setScanId(Integer scanId) {
        this.scanId = scanId;
    }

    public Integer getFilesScan() {
        return filesScan;
    }

    public void setFilesScan(Integer filesScan) {
        this.filesScan = filesScan;
    }


    @Override
    public String toString() {
        return "ScanFiles{" +
            "id=" + id +
            ", idScan=" + scanId +
            ", filesScan=" + filesScan +
            '}';
        
    }
}
