package fr.ensai.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.NotBlank;



@Entity
@Table(name = "scan")
public class Scan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer scanid;

    @NotBlank(message = "Le type de scan ne peut pas être vide")
    private String scanType;

    @NotBlank(message = "Le dossier racine ne peut pas être vide")
    private String rootFolder;

    @NotNull(message = "Le nombre maximum de fichiers ne peut pas être vide")
    private Integer nbMaxFiles;

    @NotNull(message = "La profondeur maximale ne peut pas être vide")
    private Integer nbMaxDepth;

    private LocalDateTime scanDate;

    private String nameFilter;

    private String typeFilter;


    private String scanStatus;


    public Scan() {
    }

    public Scan(Integer id, String scanType, String rootFolder, Integer nbMaxFiles, Integer nbMaxDepth, LocalDateTime scanDate, String nameFilter, String typeFilter, String scanStatus) {
        this.scanid = id;
        this.scanType = scanType;
        this.rootFolder = rootFolder;
        this.nbMaxFiles = nbMaxFiles;
        this.nbMaxDepth = nbMaxDepth;
        this.scanDate = scanDate;
        this.nameFilter = nameFilter;
        this.typeFilter = typeFilter;
        this.scanStatus = scanStatus;
    }

    public Scan(String scanType, String rootFolder, Integer nbMaxFiles, Integer nbMaxDepth, LocalDateTime scanDate, String nameFilter, String typeFilter, String scanStatus) {
        this.scanType = scanType;
        this.rootFolder = rootFolder;
        this.nbMaxFiles = nbMaxFiles;
        this.nbMaxDepth = nbMaxDepth;
        this.scanDate = scanDate;
        this.nameFilter = nameFilter;
        this.typeFilter = typeFilter;
        this.scanStatus = scanStatus;
    }

    public Scan(String scanType, String rootFolder, Integer nbMaxFiles, Integer nbMaxDepth, String nameFilter, String typeFilter) {
        this.scanType = scanType;
        this.rootFolder = rootFolder;
        this.nbMaxFiles = nbMaxFiles;
        this.nbMaxDepth = nbMaxDepth;
        this.nameFilter = nameFilter;
        this.typeFilter = typeFilter;
    }

    public Scan(Scan oldScan) {
        this.scanType = oldScan.getScanType();
        this.rootFolder = oldScan.getRootFolder();
        this.nbMaxFiles = oldScan.getNbMaxFiles();
        this.nbMaxDepth = oldScan.getNbMaxDepth();
        this.scanDate = oldScan.getScanDate();
        this.nameFilter = oldScan.getNameFilter();
        this.typeFilter = oldScan.getTypeFilter();
        this.scanStatus = oldScan.getScanStatus();
    }
    // Getters and Setters

    public Integer getId() {
        return this.scanid;
    }

    public void setId(Integer id) {
        this.scanid = id;
    }

    public String getScanType() {
        return this.scanType;
    }

    public void setScanType(String scanType) {
        this.scanType = scanType;
    }

    public String getRootFolder() {
        return this.rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Integer getNbMaxFiles() {
        return this.nbMaxFiles;
    }

    public void setNbMaxFiles(Integer nbMaxFiles) {
        this.nbMaxFiles = nbMaxFiles;
    }

    public Integer getNbMaxDepth() {
        return this.nbMaxDepth;
    }

    public void setNbMaxDepth(Integer nbMaxDepth) {
        this.nbMaxDepth = nbMaxDepth;
    }

    public LocalDateTime getScanDate() {
        return this.scanDate;
    }

    public void setScanDate(LocalDateTime scanDate) {
        this.scanDate = scanDate;
    }

    public String getNameFilter() {
        return this.nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public String getTypeFilter() {
        return this.typeFilter;
    }

    public void setTypeFilter(String typeFilter) {
        this.typeFilter = typeFilter;
    }

    public String getScanStatus() {
        return this.scanStatus;
    }

    public void setScanStatus(String scanStatus) {
        this.scanStatus = scanStatus;
    }

    @Override
    public String toString() {
        return "Scan{" +
            "id=" + scanid +
            ", scanType='" + scanType + '\'' +
            ", rootFolder='" + rootFolder + '\'' +
            ", nbMaxFiles=" + nbMaxFiles +
            ", nbMaxDepth=" + nbMaxDepth +
            ", scanDate=" + scanDate +
            ", scanStatus='" + scanStatus + '\'' +
            '}';
    }

}
