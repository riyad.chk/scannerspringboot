package fr.ensai.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import java.time.Duration;
@Entity
@Table(name = "scan_stats")
public class ScanStats {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer idScan;

    private Duration duration;

    private Long nbFiles;

    private Long nbFolders;

    private Double meanDurationFile;

    private Double meanDurationFolder;

    public ScanStats() {
    }

    public ScanStats(Integer id, Integer idScan, Duration duration, Long nbFiles, Long nbFolders, Double meanDurationFile, Double meanDurationFolder) {
        this.id = id;
        this.idScan = idScan;
        this.duration = duration;
        this.nbFiles = nbFiles;
        this.nbFolders = nbFolders;
        this.meanDurationFile = meanDurationFile;
        this.meanDurationFolder = meanDurationFolder;
    }

    public ScanStats(Integer idScan, Duration duration, Long nbFiles, Long nbFolders, Double meanDurationFile, Double meanDurationFolder) {
        this.idScan = idScan;
        this.duration = duration;
        this.nbFiles = nbFiles;
        this.nbFolders = nbFolders;
        this.meanDurationFile = meanDurationFile;
        this.meanDurationFolder = meanDurationFolder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdScan() {
        return idScan;
    }

    public void setIdScan(Integer idScan) {
        this.idScan = idScan;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Long getNbFiles() {
        return nbFiles;
    }

    public void setNbFiles(Long nbFiles) {
        this.nbFiles = nbFiles;
    }

    public Long getNbFolders() {
        return nbFolders;
    }

    public void setNbFolders(Long nbFolders) {
        this.nbFolders = nbFolders;
    }

    public Double getMeanDurationFile() {
        return meanDurationFile;
    }

    public void setMeanDurationFile(Double meanDurationFile) {
        this.meanDurationFile = meanDurationFile;
    }

    public Double getMeanDurationFolder() {
        return meanDurationFolder;
    }

    public void setMeanDurationFolder(Double meanDurationFolder) {
        this.meanDurationFolder = meanDurationFolder;
    }


    @Override
    public String toString() {
        // Format duration from nanoseconds to seconds with 3 decimal places
        double durationInSeconds = duration.getSeconds() + duration.getNano() / 1_000_000_000.0;
        String formattedDuration = String.format("%.3f seconds", durationInSeconds);
    
        // Format mean durations from nanoseconds to microseconds with 2 decimal places for better readability
        String formattedMeanDurationFile = String.format("%.2f microseconds", meanDurationFile / 1_000.0);
        String formattedMeanDurationFolder = String.format("%.2f microseconds", meanDurationFolder / 1_000.0);
    
        return "ScanStats{" +
               "idScan=" + idScan +
               ", duration=" + formattedDuration +
               ", nbFiles=" + nbFiles +
               ", nbFolders=" + nbFolders +
               ", meanDurationFile=" + formattedMeanDurationFile +
               ", meanDurationFolder=" + formattedMeanDurationFolder +
               '}';
    }
    
}
