package fr.ensai.demo.controller;

import java.util.List;
import fr.ensai.demo.model.Files;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.ScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/scans")
public class ScanController {

    @Autowired
    private ScanService scanService;

    @PostMapping("/create")
    public ResponseEntity<Scan> createScan(@Valid @RequestBody Scan scanDetails) {
        // Assuming scanDetails contains all necessary info for initiating a scan
        Scan newScan = scanService.initiateScan(
            scanDetails.getRootFolder(),
            scanDetails.getNbMaxDepth(),
            scanDetails.getNbMaxFiles(),
            scanDetails.getNameFilter(),
            scanDetails.getTypeFilter(),
            scanDetails.getScanType()
        );
        return ResponseEntity.ok(newScan);
    }

    @PostMapping("/duplicate/{scanId}")
    public ResponseEntity<Scan> duplicateScan(@Valid @PathVariable Integer scanId, @RequestBody Scan scanModifications) {
        Scan duplicatedScan = scanService.duplicateScan(scanId, scanModifications);
        return ResponseEntity.ok(duplicatedScan);
    }
    
    @DeleteMapping("/delete/{scanId}")
    public ResponseEntity<?> deleteScan(@Valid @PathVariable Integer scanId) {
        scanService.deleteScan(scanId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/replay/{scanId}")
    public ResponseEntity<Scan> replayScan(@Valid @PathVariable Integer scanId) {
        Scan replayedScan = scanService.replayScan(scanId);
        return ResponseEntity.ok(replayedScan);
    }

    @GetMapping("/compare/{scanId1}/{scanId2}")
    public ResponseEntity<?> compareScans(@Valid @PathVariable Integer scanId1, @PathVariable Integer scanId2) {
        String comparisonResult = scanService.compareScans(scanId1, scanId2);
        return ResponseEntity.ok(comparisonResult);
    }

    @GetMapping("/scan/{scanId}")
    public ResponseEntity<Scan> getScanById(@PathVariable Integer scanId) {
        Scan scan = scanService.getScanById(scanId);
        if (scan == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(scan);
    }

    @GetMapping("/stats")
    public ResponseEntity<?> getScanStats() {
        String stats = scanService.getScanStats();
        return ResponseEntity.ok(stats);
    }

    @GetMapping("/files/{fileId}")
    public ResponseEntity<Files> getFileById(@PathVariable Long fileId) {
        Files file = scanService.getFileById(fileId);
        if (file == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(file);
    }

    @GetMapping("/scan/{scanId}/files")
    public ResponseEntity<List<Files>> getFilesByScanId(@PathVariable Integer scanId) {
        List<Files> files = scanService.getFilesByScanId(scanId);
        if (files.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(files);
    }
    

    
}
