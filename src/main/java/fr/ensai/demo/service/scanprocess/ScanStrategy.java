package fr.ensai.demo.service.scanprocess;

public interface ScanStrategy {

    public void scan(String parentDirectory, String nbMaxFiles, String nbMaxDepth, String nameFilter, String typeFilter);

}
