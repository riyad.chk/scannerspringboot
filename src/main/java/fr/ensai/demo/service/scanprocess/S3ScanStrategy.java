package fr.ensai.demo.service.scanprocess;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class S3ScanStrategy implements ScanStrategy {

    private AmazonS3 s3client;

    private List<ScanResult> result;

    public S3ScanStrategy() {
        this.s3client = AmazonS3ClientBuilder.standard().build();
        this.result = new ArrayList<>();
    }

    @Override
    public void scan(String bucketName, String objectKey, String nbMaxDepth, String nameFilter, String typeFilter) {
        long startTime = System.nanoTime();

        S3Object object = s3client.getObject(new GetObjectRequest(bucketName, objectKey));
        long size = object.getObjectMetadata().getContentLength();
        LocalDateTime lastModified = LocalDateTime.ofInstant(object.getObjectMetadata().getLastModified().toInstant(), ZoneId.systemDefault());

        long duration = System.nanoTime() - startTime;

        this.result.add(new ScanResult(size, duration, objectKey, objectKey, lastModified, null));
    }
        

    public List<ScanResult> totalScan(String bucketName, String prefix, int maxKeys) {
        ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(prefix).withMaxKeys(maxKeys);
        ListObjectsV2Result result = s3client.listObjectsV2(req);

        List<ScanResult> scanResults = new ArrayList<ScanResult>();
        for (S3ObjectSummary summary : result.getObjectSummaries()) {
            scanResults.add(new ScanResult(summary.getSize(), 0, summary.getKey(), summary.getKey(), LocalDateTime.ofInstant(summary.getLastModified().toInstant(), ZoneId.systemDefault()), null));
        }

        return scanResults;
    }
}
