package fr.ensai.demo.service.scanprocess;

import java.time.LocalDateTime;


public class ScanResult {
    private long size; // Size of the file or directory
    private long scanDuration; // Duration of the scan in nanoseconds
    private String name; // Name of the file or directory
    private String path; // Path of the file or directory
    private LocalDateTime lastModified; // Last modified time
    private String type; // Type of the file or directory

    public ScanResult(long size, long scanDuration, String name, String path, LocalDateTime lastModified, String type) {
        this.size = size;
        this.scanDuration = scanDuration;
        this.name = name;
        this.path = path;
        this.lastModified = lastModified;
        this.type = type;
    }

    // Getters and setters
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getScanDuration() {
        return scanDuration;
    }

    public void setScanDuration(long scanDuration) {
        this.scanDuration = scanDuration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
