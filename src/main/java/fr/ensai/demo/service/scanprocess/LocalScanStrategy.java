package fr.ensai.demo.service.scanprocess;

import java.io.File;


public class LocalScanStrategy implements ScanStrategy {

    @Override
    public void scan(String parentDirectory, String nbMaxFiles, String nbMaxDepth, String nameFilter, String typeFilter) {
        FolderComposite rootFolder = new FolderComposite(new File(parentDirectory));
        rootFolder.scan(Integer.parseInt(nbMaxFiles), Integer.parseInt(nbMaxDepth), nameFilter, typeFilter);
    }
}
