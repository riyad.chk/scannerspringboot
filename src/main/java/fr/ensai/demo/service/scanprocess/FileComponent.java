package fr.ensai.demo.service.scanprocess;

import java.util.List;

import java.io.File;

public abstract class FileComponent {
    protected File file;

    public FileComponent(File file) {
        this.file = file;
    }

    public abstract List<ScanResult> scan(int nbMaxFiles, int nbMaxDepth, String nameFilter, String typeFilter);
}
