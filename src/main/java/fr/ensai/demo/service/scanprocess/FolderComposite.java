package fr.ensai.demo.service.scanprocess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class FolderComposite extends FileComponent {
    private List<FileComponent> children = new ArrayList<>();

    public FolderComposite(File file) {
        super(file);
        for (File childFile : file.listFiles()) {
            if (childFile.isDirectory()) {
                children.add(new FolderComposite(childFile));
            } else {
                children.add(new FileLeaf(childFile));
            }
        }
    }

    @Override
    public List<ScanResult> scan(int nbMaxFiles, int nbMaxDepth, String nameFilter, String typeFilter) {
        long startTime = System.nanoTime();
        List<ScanResult> childResults = new ArrayList<>();
        if (nbMaxDepth == 0 || nbMaxFiles <= 0) {
            return List.of(new ScanResult(0, System.nanoTime() - startTime, file.getName(), file.getAbsolutePath(), null, "directory"));
        }

        for (FileComponent child : children) {
            List<ScanResult> results = child.scan(nbMaxFiles, nbMaxDepth - 1, nameFilter, typeFilter);
            childResults.addAll(results);
            // Only decrement the max files count for files, not directories
            if (child instanceof FileLeaf) {
                nbMaxFiles--;
                if (nbMaxFiles <= 0) break;
            }
        }

        long duration = System.nanoTime() - startTime;

        // Aggregating the results for this folder
        LocalDateTime lastModifiedTime = null;
        try {
            BasicFileAttributes attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            lastModifiedTime = LocalDateTime.ofInstant(
                    Instant.ofEpochMilli(attrs.lastModifiedTime().toMillis()),
                    ZoneId.systemDefault()
            );
        } catch (IOException e) {
            System.err.println("Error reading directory attributes: " + e.getMessage());
        }
        childResults.add(0, new ScanResult(0, duration, file.getName(), file.getParent(), lastModifiedTime, "directory"));
        return childResults;
    }
}
