package fr.ensai.demo.service.scanprocess;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class FileLeaf extends FileComponent {

    public FileLeaf(File file) {
        super(file);
    }

    @Override
    public List<ScanResult> scan(int nbMaxFiles, int nbMaxDepth, String nameFilter, String typeFilter) {
        long startTime = System.nanoTime();
        List<ScanResult> listResults = new ArrayList<>();

        if ((nameFilter != null && !file.getName().matches(nameFilter)) || 
            (typeFilter != null && !file.getName().endsWith(typeFilter))) {
            return listResults; // Retourne une liste vide si le fichier ne correspond pas aux filtres
        }

        try {
            BasicFileAttributes attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            LocalDateTime lastModifiedTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(file.lastModified()),
                ZoneId.systemDefault());

            String fileType = Files.probeContentType(file.toPath()); // If the file type is not recognized, take the extension as type
            fileType = (fileType != null) ? fileType : file.getName().substring(file.getName().lastIndexOf('.') + 1);

                long duration = System.nanoTime() - startTime;
                String parentFolderPath = file.getParent();
                listResults.add(new ScanResult(attrs.size(), duration, file.getName(), parentFolderPath, lastModifiedTime, fileType));
            } catch (IOException e) {
                long duration = System.nanoTime() - startTime;
                System.err.println("Error reading file attributes: " + e.getMessage());
                listResults.add(new ScanResult(0, duration, file.getName(), null, null, "unknown")); // Utilise "unknown" pour fileType si une erreur se produit
            }

        return listResults;
    }
}
