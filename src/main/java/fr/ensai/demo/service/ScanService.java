
package fr.ensai.demo.service;

import fr.ensai.demo.model.*;
import fr.ensai.demo.repository.*;
import fr.ensai.demo.service.scanprocess.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScanService {

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private FilesRepository filesRepository;

    @Autowired
    private ScanFilesRepository scanFilesRepository;

    @Autowired
    private ScanStatsRepository scanStatsRepository;

    public Scan initiateScan(String rootPath, int maxDepth, int maxFiles, String nameFilter, String typeFilter, String scanType) {
        // Create and save the initial scan configuration
        Scan scan = new Scan();
        scan.setScanType(scanType);
        scan.setRootFolder(rootPath);
        scan.setNbMaxFiles(maxFiles);
        scan.setNbMaxDepth(maxDepth);
        scan.setNameFilter(nameFilter);
        scan.setTypeFilter(typeFilter);
        scan.setScanDate(java.time.LocalDateTime.now());
        scan.setScanStatus("STARTED");
        Scan savedScan = scanRepository.save(scan);
        //Check that rootPath is a valid directory
        File root = new File(rootPath);
        if (!root.exists() || !root.isDirectory()) {
            // Update scan status to FAILED and save
            savedScan.setScanStatus("FAILED");
            scanRepository.save(scan);
            return savedScan;
        }
        // Perform the actual scanning process
        FileComponent rootFolder = new FolderComposite(new File(rootPath));
        // The scan method should now return a list of ScanResults if you're scanning multiple files
        List<ScanResult> results = rootFolder.scan(maxFiles, maxDepth, nameFilter, typeFilter);

        // Compute statistics for the scan and store them
        ScanStats stats = new ScanStats();
        stats.setIdScan(savedScan.getId());
        stats.setMeanDurationFile(results.stream()
            .filter(result -> !result.getType().equals("directory"))
            .mapToLong(ScanResult::getScanDuration)
            .average()
            .orElse(0));

        stats.setMeanDurationFolder(results.stream()
            .filter(result -> result.getType().equals("directory"))
            .mapToLong(ScanResult::getScanDuration)
            .average()
            .orElse(0));
        stats.setNbFiles(results.stream()
            .filter(result -> !result.getType().equals("directory"))
            .count());
        stats.setNbFolders(results.stream()
            .filter(result -> result.getType().equals("directory"))
            .count());
        stats.setDuration(java.time.Duration.ofNanos(results.stream()
            .mapToLong(ScanResult::getScanDuration)
            .sum()));

        scanStatsRepository.save(stats);

        // Store the results of the scan
        results.forEach(result -> storeScanResults(savedScan, result));

        // Update scan status to COMPLETED and save
        savedScan.setScanStatus("COMPLETED");
        scanRepository.save(scan);

        return scan;
    }

    private void storeScanResults(Scan scan, ScanResult result) {
        // Store individual file details
        Files fileRecord = new Files();
        fileRecord.setNamefile(result.getName());
        fileRecord.setSizefile((int) result.getSize());
        fileRecord.setTypefile(result.getType());
        if (result.getLastModified() != null) {
            fileRecord.setLastmodified(result.getLastModified().atZone(ZoneId.systemDefault()).toLocalDateTime());
        } else {
            fileRecord.setLastmodified(null); // or set a default value or handle the case according to your application logic
        }
        fileRecord.setParentfolder(result.getPath());
        filesRepository.save(fileRecord);

        // Link scanned files with the scan
        ScanFiles scanFile = new ScanFiles();
        scanFile.setScanId(scan.getId());
        scanFile.setFilesScan(fileRecord.getId().intValue());
        scanFilesRepository.save(scanFile);
    
    
    }

    public Scan createScan(Scan scanDetails) {
        scanDetails.setScanStatus("CREATED");
        return scanRepository.save(scanDetails);
    }

    public Scan duplicateScan(Integer scanId, Scan scanModifications) {
        // Trouver le scan original et le dupliquer avec les modifications
        Scan originalScan = scanRepository.findById(scanId).orElse(null);
        if (originalScan == null) {
            return null; // Ou gérer l'erreur selon votre logique d'application
        }
    
        // Création d'un nouveau scan basé sur l'original
        Scan newScan = new Scan();
        // Copier les valeurs, en vérifiant null si nécessaire
        newScan.setScanType(originalScan.getScanType());
        newScan.setRootFolder(originalScan.getRootFolder());
        newScan.setNameFilter(scanModifications.getNameFilter() != null ? scanModifications.getNameFilter() : originalScan.getNameFilter());
        newScan.setTypeFilter(scanModifications.getTypeFilter() != null ? scanModifications.getTypeFilter() : originalScan.getTypeFilter());
        newScan.setNbMaxDepth(scanModifications.getNbMaxDepth() != null ? scanModifications.getNbMaxDepth() : originalScan.getNbMaxDepth());
        newScan.setNbMaxFiles(scanModifications.getNbMaxFiles() != null ? scanModifications.getNbMaxFiles() : originalScan.getNbMaxFiles());
    
        // Assurez-vous que les champs nécessaires sont initialisés
        if (newScan.getNbMaxFiles() == null) {
            newScan.setNbMaxFiles(0); // Ou une autre valeur par défaut appropriée
        }
    
        // Lancer un nouveau scan basé sur le scan dupliqué
        return initiateScan(newScan.getRootFolder(),
                            newScan.getNbMaxDepth(),
                            newScan.getNbMaxFiles(),
                            newScan.getNameFilter(),
                            newScan.getTypeFilter(),
                            newScan.getScanType());
    }
    

    public void deleteScan(Integer scanId) {
        // Supprimer le scan de la base de données
        scanRepository.deleteById(scanId);
    }

    public Scan getScanById(Integer scanId) {
        return scanRepository.findById(scanId).orElse(null);
    }

    public Scan replayScan(Integer scanId) {
        // Rejouer le scan avec les mêmes paramètres
        Scan originalScan = scanRepository.findById(scanId).orElse(null);
        if (originalScan == null) {
            // Gérer l'absence du scan original
            return null;
        }
        return initiateScan(originalScan.getRootFolder(),
                            originalScan.getNbMaxDepth(),
                            originalScan.getNbMaxFiles(),
                            originalScan.getNameFilter(),
                            originalScan.getTypeFilter(),
                            originalScan.getScanType());
    }

    public String compareScans(Integer scanId1, Integer scanId2) {
        Scan scan1 = scanRepository.findById(scanId1).orElse(null);
        Scan scan2 = scanRepository.findById(scanId2).orElse(null);
        if (scan1 == null || scan2 == null) {
            return "Un des scans est introuvable.";
        }
        
        List<ScanFiles> scanFiles1 = scanFilesRepository.findByScanId(scanId1);
        List<ScanFiles> scanFiles2 = scanFilesRepository.findByScanId(scanId2);
        
        StringBuilder sb = new StringBuilder();
        sb.append("Scan 1: ").append(scan1.toString()).append("\n");
        sb.append("Scan 2: ").append(scan2.toString()).append("\n");
        sb.append("Scan 1 files:\n");
        scanFiles1.forEach(file -> sb.append(file.toString()).append("\n"));
        sb.append("Scan 2 files:\n");
        scanFiles2.forEach(file -> sb.append(file.toString()).append("\n"));
        
        return sb.toString();
    }
    

    public String getScanStats() {
        Iterable<ScanStats> allStats = scanStatsRepository.findAll();
        StringBuilder sb = new StringBuilder();
        allStats.forEach(stat -> sb.append(stat.toString()).append("\n"));
        return sb.toString();
    }
    
    public Files getFileById(Long fileId) {
        return filesRepository.findById(fileId).orElse(null);
    }


    public List<Files> getFilesByScanId(Integer scanId) {
        List<ScanFiles> scanFiles = scanFilesRepository.findByScanId(scanId);
        List<Files> files = new ArrayList<>();
        for (ScanFiles scanFile : scanFiles) {
            filesRepository.findById(scanFile.getFilesScan().longValue()).ifPresent(files::add);
        }
        return files;
    }


}


