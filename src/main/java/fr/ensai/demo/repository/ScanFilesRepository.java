package fr.ensai.demo.repository;

import fr.ensai.demo.model.ScanFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ScanFilesRepository extends CrudRepository<ScanFiles, Integer> {

    // Method names should match the case of the fields in the ScanFiles entity
    List<ScanFiles> findByScanId(Integer scanId);
    List<ScanFiles> findByFilesScan(Integer filesScan);

    // Additional custom query methods can be added here as needed
}
