package fr.ensai.demo.repository;

import fr.ensai.demo.model.Scan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ScanRepository extends CrudRepository<Scan, Integer> {
    public List<Scan> findByScanid(Integer scanid);
    public List<Scan> findByScanType(String scanType);
    public List<Scan> findByRootFolder(String rootFolder);
    public List<Scan> findByScanStatus(String scanStatus);
}