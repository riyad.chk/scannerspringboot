package fr.ensai.demo.repository;

import fr.ensai.demo.model.Files;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilesRepository extends CrudRepository<Files, Long> {

    // Correct method name to match the field 'filesid' in the 'Files' entity
    public Iterable<Files> findByFilesid(Long filesid);

    // Other methods as before
    public Iterable<Files> findByNamefile(String namefile);
    public Iterable<Files> findByTypefile(String typefile);
    public Iterable<Files> findByParentfolder(String parentfolder);

}
