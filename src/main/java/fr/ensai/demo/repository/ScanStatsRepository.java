package fr.ensai.demo.repository;

import fr.ensai.demo.model.ScanStats;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ScanStatsRepository extends CrudRepository<ScanStats, Integer> {

    public List<ScanStats> findByIdScan(Integer idScan);

    // 

}
