package fr.ensai.demo.controller;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.ScanService;

import org.checkerframework.checker.units.qual.m;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import java.util.ArrayList;

import java.nio.file.Path;
import java.io.File;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get; // Import the necessary class
import fr.ensai.demo.model.Files;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ScanController.class)
class ScanControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScanService scanService;

    @TempDir
    Path tempDir;

    @BeforeEach
    void setUp() throws Exception {
        // Rempli le dossier temporaire avec des sous-dossiers et fichiers
        for (int i = 0; i < 20; i++) {
            File subDir = tempDir.resolve("subdir" + i).toFile();
            subDir.mkdir();
            for (int j = 0; j < 3; j++) {
                File file = new File(subDir, "file" + j + ".txt");
                file.createNewFile();
            }
        }

        Scan mockScan = new Scan();
        mockScan.setScanType("local");
        mockScan.setRootFolder(tempDir.toString());
        mockScan.setNbMaxFiles(100);
        mockScan.setNbMaxDepth(10);
        mockScan.setNameFilter(".*");
        mockScan.setTypeFilter("");
        given(scanService.initiateScan(
                mockScan.getRootFolder(),
                mockScan.getNbMaxDepth(),
                mockScan.getNbMaxFiles(),
                mockScan.getNameFilter(),
                mockScan.getTypeFilter(),
                mockScan.getScanType()
        )).willReturn(mockScan);
    }

    @Test
    void createScan_ShouldReturnScan_WhenRequestIsValid() throws Exception {
        mockMvc.perform(post("/scans/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"scanType\": \"local\", \"rootFolder\": \"" + tempDir.toString() + "\", \"nbMaxFiles\": 100, \"nbMaxDepth\": 10, \"nameFilter\": \".*\", \"typeFilter\": \"\" }"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                // Ajoutez d'autres vérifications si nécessaire
                .andExpect(jsonPath("$.scanType").value("local"));
    }


    @Test
    void createScan_ShouldReturnBadRequest_WhenRequestIsInvalid() throws Exception {
        mockMvc.perform(post("/scans/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"scanType\": \"local\", \"rootFolder\": \"\", \"nbMaxFiles\": 100, \"nbMaxDepth\": 10, \"nameFilter\": \".*\", \"typeFilter\": \"\" }"))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    void duplicateScan_ShouldReturnScan_WhenRequestIsValid() throws Exception {
        Scan mockScan = new Scan();
        mockScan.setScanType("local");
        mockScan.setRootFolder(tempDir.toString());
        mockScan.setNbMaxFiles(100);
        mockScan.setNbMaxDepth(10);
        mockScan.setNameFilter(".*");
        mockScan.setTypeFilter("");
        given(scanService.duplicateScan(mockScan.getId(), mockScan)).willReturn(mockScan);

        mockMvc.perform(post("/scans/duplicate/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"scanType\": \"local\", \"rootFolder\": \"" + tempDir.toString() + "\", \"nbMaxFiles\": 100, \"nbMaxDepth\": 10, \"nameFilter\": \".*\", \"typeFilter\": \"\" }"))
                .andExpect(status().isOk());
    }


    @Test
    void replayScan_ShouldReturnScan_WhenRequestIsValid() throws Exception {
        Scan mockScan = new Scan();
        mockScan.setScanType("local");
        mockScan.setRootFolder(tempDir.toString());
        mockScan.setNbMaxFiles(100);
        mockScan.setNbMaxDepth(10);
        mockScan.setNameFilter(".*");
        mockScan.setTypeFilter("");
        given(scanService.replayScan(1)).willReturn(mockScan);

        mockMvc.perform(post("/scans/replay/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.scanType").value("local"));
    }

    @Test
    void compareScans_ShouldReturnComparisonResult_WhenRequestIsValid() throws Exception {
        given(scanService.compareScans(1, 2)).willReturn("Comparison result");

        mockMvc.perform(get("/scans/compare/1/2")) // Add the missing method call
                .andExpect(status().isOk())
                .andExpect(content().string("Comparison result"));
    }

    @Test
    void getScanStats_ShouldReturnStats_WhenRequestIsValid() throws Exception {
        given(scanService.getScanStats()).willReturn("Stats");

        mockMvc.perform(get("/scans/stats"))
                .andExpect(status().isOk())
                .andExpect(content().string("Stats"));
    }

    @Test
    void getFileById_ShouldReturnFile_WhenFileExists() throws Exception {
        Files mockFile = new Files();
        mockFile.setId(1L);
        given(scanService.getFileById(1L)).willReturn(mockFile);

        mockMvc.perform(get("/scans/files/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    void getFilesByScanId_ShouldReturnFiles_WhenFilesExist() throws Exception {
        List<Files> mockFiles = new ArrayList<>();
        Files mockFile = new Files();
        mockFile.setId(1L);
        mockFiles.add(mockFile);
        given(scanService.getFilesByScanId(1)).willReturn(mockFiles);

        mockMvc.perform(get("/scans/scan/1/files"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1L));
    }
}
