package fr.ensai.demo.service;

import fr.ensai.demo.model.*;
import fr.ensai.demo.repository.*;
import fr.ensai.demo.service.scanprocess.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ScanServiceTest {

    @Mock
    private ScanRepository scanRepository;

    @Mock
    private FilesRepository filesRepository;

    @Mock
    private ScanFilesRepository scanFilesRepository;

    @Mock
    private ScanStatsRepository scanStatsRepository;

    @InjectMocks
    private ScanService scanService;

    @TempDir
    Path tempDir;

    @BeforeEach
    // create a temporary directory with t20 emp subdirectories and 3 files par subdirectory with different extensions
    void createTempDir() {
        for (int i = 0; i < 20; i++) {
            File subDir = new File(tempDir.toString() + "/subdir" + i);
            subDir.mkdir();
            for (int j = 0; j < 3; j++) {
                File file = new File(subDir.toString() + "/file" + j + ".txt");
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        
        // Mock Scan object with an ID
        Scan mockScan = new Scan();
        mockScan.setId(1); // Use Integer for ID
        mockScan.setRootFolder(tempDir.toString());
        mockScan.setNbMaxDepth(1);
        mockScan.setNbMaxFiles(100);
        mockScan.setScanType("local");
        mockScan.setScanDate(LocalDateTime.now());
        mockScan.setScanStatus("STARTED");
    
        when(scanRepository.save(any(Scan.class))).thenReturn(mockScan);
    
        // Mock Files object to return with an ID when saved
        when(filesRepository.save(any(Files.class))).thenAnswer(invocation -> {
            Files files = invocation.getArgument(0);
            if (files.getId() == null) files.setId(1L); // Set ID as Long
            return files;
        });
    }
    


    @Test
    void initiateScan_ShouldCreateAndSaveScan_WhenGivenValidInputs() {
        // Given
        String rootPath = tempDir.toString();
        int maxDepth = 1;
        int maxFiles = 100;
        String nameFilter = ".*";
        String typeFilter = ".*";
        String scanType = "local";
        
        // When
        Scan resultScan = scanService.initiateScan(rootPath, maxDepth, maxFiles, nameFilter, typeFilter, scanType);
        // Then
        verify(scanRepository, times(2)).save(any(Scan.class));
        assertNotNull(resultScan);
    }

    @Test
    void initiateScan_ShouldCreateAndSaveFiles_WhenGivenValidInputs() {
        // Given
        String rootPath = tempDir.toString();
        int maxDepth = 1;
        int maxFiles = 100;
        String nameFilter = ".*";
        String typeFilter = ".*";
        String scanType = "local";
        
        // When
        Scan resultScan = scanService.initiateScan(rootPath, maxDepth, maxFiles, nameFilter, typeFilter, scanType);
        // Then
        verify(filesRepository, times(21)).save(any(Files.class));
        assertNotNull(resultScan);
    }

    @Test
    void initiateScan_ShouldCreateAndSaveScanStats_WhenGivenValidInputs() {
        // Given
        String rootPath = tempDir.toString();
        int maxDepth = 1;
        int maxFiles = 100;
        String nameFilter = ".*";
        String typeFilter = ".*";
        String scanType = "local";
        
        // When
        Scan resultScan = scanService.initiateScan(rootPath, maxDepth, maxFiles, nameFilter, typeFilter, scanType);
        // Then
        verify(scanStatsRepository, times(1)).save(any(ScanStats.class));
        assertNotNull(resultScan);
    }

    @Test
    void initiateScan_ShouldCreateAndSaveScanFiles_WhenGivenValidInputs() {
        // Given (the directory is not empty)
        String rootPath = tempDir.toString();
        int maxDepth = 1;
        int maxFiles = 100;
        String nameFilter = ".*";
        String typeFilter = ".*";
        String scanType = "local";
        
        // When
        Scan resultScan = scanService.initiateScan(rootPath, maxDepth, maxFiles, nameFilter, typeFilter, scanType);
        // Then
        verify(scanFilesRepository, times(21)).save(any(ScanFiles.class));
        assertNotNull(resultScan);
    }

    @Test
    void createScan_ShouldSaveScanWithCreatedStatus() {
        // Arrange
        Scan scanDetails = new Scan();
        scanDetails.setScanType("local");
        scanDetails.setRootFolder(tempDir.toString()); 
        scanDetails.setNbMaxFiles(100);
        scanDetails.setNbMaxDepth(10);
        scanDetails.setNameFilter(".*");
        scanDetails.setTypeFilter("");

        when(scanRepository.save(any(Scan.class))).thenAnswer(i -> i.getArguments()[0]);

        // Act
        Scan savedScan = scanService.createScan(scanDetails);

        // Assert
        assertNotNull(savedScan);
        assertEquals("CREATED", savedScan.getScanStatus());
        verify(scanRepository).save(scanDetails);
    }

    @Test
    void duplicateScan_ShouldDuplicateAndInitiateScan_WhenOriginalScanExists() {
        // Given
        Scan originalScan = new Scan();
        originalScan.setId(1);
        originalScan.setScanType("local");
        originalScan.setRootFolder(tempDir.toString());
        originalScan.setNbMaxFiles(100);
        originalScan.setNbMaxDepth(10);
        originalScan.setNameFilter(".*");
        originalScan.setTypeFilter("");

        when(scanRepository.findById(1)).thenReturn(Optional.of(originalScan));
        when(scanRepository.save(any(Scan.class))).thenAnswer(i -> i.getArguments()[0]);

        // When
        Scan duplicatedScan = scanService.duplicateScan(1, originalScan);
        
        // Then
        assertNotNull(duplicatedScan);
        verify(scanRepository, times(2)).save(any(Scan.class));

    
    }
    

}