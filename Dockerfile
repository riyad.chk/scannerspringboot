FROM  maven:3.8.8-eclipse-temurin-17

COPY target/demo-0.0.1-SNAPSHOT.jar /app.jar

CMD ["java", "-jar", "/app.jar"]
